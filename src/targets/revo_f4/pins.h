#ifndef PINS_H
#define PINS_H

#include "PinNames.h"

#ifdef __cplusplus
extern "C" {
#endif

namespace targets {
namespace revo_f4 {

typedef enum
{
    // Onboard leds
    PIN_BLUE_LED   = PB_5,
    PIN_ORANGE_LED = PB_4,

    // Analog sensors
    PIN_VOLTAGE_SENSOR = PC_2,
    PIN_CURRENT_SENSOR = PC_1,

    // MPU SPI pins
    PIN_MPU_MOSI = PA_7,
    PIN_MPU_MISO = PA_6,
    PIN_MPU_SCLK = PA_5,
    PIN_MPU_NSS  = PA_4,

    // Flash SPI pins
    PIN_FLASH_MOSI = PC_12,
    PIN_FLASH_MISO = PC_11,
    PIN_FLASH_SCLK = PC_10,
    PIN_FLASH_NSS  = PA_15,

    // Compass i2c pins
    PIN_COMPASS_SCL = PB_8,
    PIN_COMPASS_SDA = PB_9,

    // Flexiport - Needs fixing in m-bed
    // PIN_USART3_RX = PB_10,
    // PIN_USART4_TX = PB_11,

    PIN_USART2_RX = PA_3, // Servo 3
    PIN_USART2_TX = PA_2, // Servo 4

    PIN_USART4_RX = PA_1, // Servo 5
    PIN_USART4_TX = PA_0, // Servo 6

} PinMap;

} // namespace revo_f4
} // namespace targets

#ifdef __cplusplus
}
#endif

#endif