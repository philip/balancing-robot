#include "src/math/Utilities.h"

#include <stdlib.h>

namespace math {

float constrain(float value, float range)
{
    if (value < -range)
    {
        return -range;
    }
    if (value > range)
    {
        return range;
    }
    return value;
}
    
}