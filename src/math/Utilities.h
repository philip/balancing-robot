#ifndef PI
  #define PI 3.14159265358979f
#endif


namespace math {

float constrain(float value, float range);

}