#include "mbed.h"

#pragma once


namespace serialization {

//! Receive commands from the Touch OSC layout through a NodeMCU
class RCProtocol
{

public:

    struct PacketIn {
        
        int16_t Throttle;
        int16_t Steering;

        int16_t Kp;
        int16_t Ki;
        int16_t Kd;

        bool Enabled;

        uint8_t Checksum;
        
        PacketIn()
        : Throttle(0)
        , Steering(0)
        , Kp(0)
        , Ki(0)
        , Kd(0)
        , Enabled(false)
        , Checksum(0)
        {
        }

    } __attribute__ ((__packed__));

    struct PacketOut {

        int32_t MagicWord;
        
        int16_t BatteryLevel;

        int16_t Mode;

        uint8_t Checksum;

        PacketOut()
        : MagicWord(0xDEC0DED)
        , BatteryLevel(0)
        , Mode(0)
        , Checksum(0)
        {
        }

    } __attribute__ ((__packed__));

    RCProtocol();

    //! Append a byte to read until we have a hole package.
    //! Returns true when a hole package is available
    bool readByte(uint8_t newByte);

    //! Read the latest package
    RCProtocol::PacketIn readPacket();

    //! Generate a byte to write
    //! @done is set to true when a hole packet
    //! has been passed
    uint8_t writeByte(bool& done);
    
    //! Set the data to be written
    void setOutput(RCProtocol::PacketOut& packetOut);

    //Semaphore m_semaphore;

    const static uint32_t MAGIC_WORD = 0xDEC0DED;

private:

    RCProtocol::PacketIn m_packetIn;

    RCProtocol::PacketOut m_packetOut;

};

}