#include "RCProtocol.h"

namespace serialization {

namespace {

//! Example package
void testParser()
{
    static RCProtocol RCProt;

    uint8_t dummy[16];

    // Magic word 233573869 / 0xDEC0DED
    dummy[0] = 0xED; // 11101101
    dummy[1] = 0x0D; // 00001101
    dummy[2] = 0xEC; // 11101100
    dummy[3] = 0x0D; // 1101
    

    // Throttle int16 1
    dummy[4] = 0x01;
    dummy[5] = 0x00;

    // Steering int16 -1
    dummy[6] = 0xFF;
    dummy[7] = 0xFF;

    // Kp 1234 
    dummy[8] = 0xD2;
    dummy[9] = 0x04;

    // Ki -1234 
    dummy[10] = 0x2E;
    dummy[11] = 0xFB;

    // Kd 10000 
    dummy[12] = 0x10;
    dummy[13] = 0x27;

    // Bool True
    dummy[14] = 0x01;

    dummy[15] = 0x00; // !?? calculate

    RCProtocol::PacketIn pkt = *((RCProtocol::PacketIn*)(dummy+4));

    for (int i = 0; i < 16; i++)
    {
        bool newPackage = RCProt.readByte(dummy[i]);
        if (newPackage)
        {
            RCProtocol::PacketIn prot = RCProt.readPacket();
        }
    }
}

}

RCProtocol::RCProtocol()
: m_packetIn()
, m_packetOut()
//, m_semaphore(0)
{
}

RCProtocol::PacketIn RCProtocol::readPacket()
{
    //m_semaphore.wait();
    return m_packetIn;
    //m_semaphore.release();
}

bool RCProtocol::readByte(uint8_t newByte)
{
    static uint8_t header[4]; // 4 bytes for our header
    static uint32_t* pHeader32 = (uint32_t*)header;

    static const int payloadSize = sizeof(RCProtocol::PacketIn);
    static uint8_t payload[payloadSize];
    static RCProtocol::PacketIn* pPacketIn = (RCProtocol::PacketIn*)payload;

    static int byteCounter = 0;

    if (*pHeader32 == MAGIC_WORD)
    {
        payload[byteCounter++] = newByte;

        static uint16_t sum = 0;

        if (byteCounter >= payloadSize)
        {
            *pHeader32 = 0; // Unvalidate the magic word
            byteCounter = 0; // Reset the read counter

            uint8_t checksum = sum & 0xFF;
            sum = 0;

            if (checksum == pPacketIn->Checksum)
            {
                // Create package
                //m_semaphore.wait();
                m_packetIn = *((PacketIn*)payload);
                //m_semaphore.release();
                return true; // new package available
            }
        }
        sum +=newByte; // placed here not to include the checksum
    }
    else // Read header
    {
        *pHeader32 = *pHeader32 >> 8 | newByte << 8*3;
    }
    return false; // Continue parsing
}

uint8_t RCProtocol::writeByte(bool& done)
{
    static const uint8_t* pByte = (uint8_t*)&m_packetOut;

    static uint8_t i = 0;
    static const uint8_t max = sizeof(RCProtocol::PacketOut);

    uint8_t outByte = pByte[i];
    i++;
    
    i = i % max;
    
    done = (i == 0) ? true : false;

    return outByte;
}

void RCProtocol::setOutput(RCProtocol::PacketOut& packetOut)
{
    m_packetOut = packetOut;

    // Now we calculate the checksum
    uint8_t* pByte = (uint8_t*)&m_packetOut;

    uint16_t sum = 0;

    for (int i = 4; i < sizeof(RCProtocol::PacketOut) - 1; i++)
    {
        sum += pByte[i];
    }

    uint8_t checksum = sum & 0xFF;

    m_packetOut.Checksum = checksum;
}

}