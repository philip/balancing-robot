#pragma once

#include "mbed.h"

namespace drivers {

class Servo
{
public:
    
    Servo(PinName servoArm);

    void center();

    //! Will set the reset the period and center the servo
    void resetTimers();

    //! in range [-1,1]
    void setPosition(double position);

    //! changes the direction of this arm
    void invert();

    //! Will start sweeping the servo without end
    void sweep(double from, double to, double intervalInSec);

    //! The servo will nod like a gentleman
    void nod();

    //! Stops any active movement (Thread)
    void stop();

public:
    
    PwmOut m_servo;

private:

    int m_dir;

    bool m_inMovement;

    double m_latestPosition;

};

} // namespace drivers
