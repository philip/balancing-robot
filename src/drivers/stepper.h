
#pragma once

#include "mbed.h"

namespace drivers {

class Stepper
{
public:

    Stepper(PinName stepPin, 
            PinName dirPin, 
            PinName enPin);

    void init();

    void enable();

    void disable();

    bool isEnabled();

    // Direction is -1 (backward) or 1 (forward)
    void setDirection(int dir);

    int getDirection();

    float limitAcceleration(float DPS);

    // Steps per second? / deg per second?
    void setSpeed(const float& DPS);

    float getSpeed();

private:

    PwmOut m_step;
    
    DigitalOut m_dir, m_en;

    bool m_accelerationLimitOn;

    float m_accelerationLimit;

    int m_stepsPerRevolution;

    int m_microStepResolution;

    int m_currentPeriod;

    int m_configuredDirection;

    int m_lastDirection;

    float m_latestSpeed;
    
};

} // namespace drivers
