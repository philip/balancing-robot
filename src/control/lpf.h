#pragma once

namespace control {

class incrementalLPF
{

public:

    incrementalLPF();

    float filter(float latestValue);

    void clear();

private:

    float m_filtered;
};


class pt1FilterApply4
{

public:

    pt1FilterApply4(float freqCut);

    float filter(float dT, float latestValue);

private:

    float m_freqCut;
    float m_RC;
    float m_filtered;
};


} // namespace control