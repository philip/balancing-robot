#include "src/control/lpf.h"

namespace control {

class controllerP
{
public:
    controllerP(float kP);

    float run(float dT, float input, float setPoint);

private:
    float m_kP;

};

class controllerPD
{
public:
    controllerPD(float kP, float kD, float saturation);

    float run(float dT, float input, float setPoint);

private:
    float m_kP;
    float m_kD;
    float m_saturation;
    float m_lastError;
    pt1FilterApply4 m_pt1FilterApply4;
};

class controllerPD2
{
public:
    controllerPD2(float kP, float kD, float saturation);

    float run(float dT, float input, float setPoint);

private:
    float m_kP;
    float m_kD;
    float m_saturation;
    float m_inputTMinus1;
    float m_inputTMinus2;
    float m_setPointTMinus1;
};

class controllerPI
{
public:
    controllerPI(float kP, float kI, float saturation, float iTermSaturation);

    float run(float dT, float input, float setPoint);

    void setGainScaling(float kPScale, float kIScale);

    void flushIntegral();

private:
    float m_kP, m_kI;
    float m_kPScale, m_kIScale;
    float m_saturation;
    float m_iTermSaturation;
    float m_integrator;
};

class controllerPID
{
public:
    controllerPID(float kP, float kI, float kD, float saturation, float iTermSaturation);

    float run(float dT, float input, float setPoint);

    //! Input values in range [-1000, 1000] will result in scales 
    //! from [0-2].
    void setGainScaling(float kPScale, float kIScale, float kDScale);

    void flushIntegral();

private:
    float m_kP, m_kI, m_kD;
    float m_kPScale, m_kIScale, m_kDScale;
    float m_saturation;
    float m_lastError;
    float m_iTermSaturation;
    float m_integrator;
    pt1FilterApply4 m_pt1FilterApply4;
};

class controllerPID2
{
public:
    controllerPID2(float kP, float kI, float kD, float saturation, float iTermSaturation);

    float run(float dT, float input, float setPoint);

    //! Input values in range [-1000, 1000] will result in scales 
    //! from [0-2].
    void setGainScaling(float kPScale, float kIScale, float kDScale);

    void flushIntegral();

private:
    float m_kP, m_kI, m_kD;
    float m_kPScale, m_kIScale, m_kDScale;
    float m_saturation;
    float m_iTermSaturation;
    float m_integrator;
    float m_inputTMinus1;
    float m_inputTMinus2;
    float m_setPointTMinus1;
};

}