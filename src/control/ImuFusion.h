#include "src/drivers/MPU6000.h"

namespace control {

class ImuFusion
{

public:
 
    ImuFusion(mpu6000_spi* pImu);

    float getAngle(float dT);

private:

    mpu6000_spi* m_pImu;

    float m_angle;


};

}