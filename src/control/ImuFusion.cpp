#include "src/control/ImuFusion.h"

namespace control {

ImuFusion::ImuFusion(mpu6000_spi* pImu)
: m_pImu(pImu)
, m_angle(0)
{
}

// TODO: Implement for all axis
float ImuFusion::getAngle(float dT)
{
    int axis = 0;

    float rot = m_pImu->read_rot(axis);

    m_angle += dT*rot;

    float ratio = 0.99; 
    //float ratio = 0.95f;
    //float ratio = 0.9996;

    float rawAngle = m_pImu->read_acc_deg(axis); // conversion from G to Deg

    m_angle = (m_angle * ratio) + (rawAngle * ((float)1.0f-ratio));

    return m_angle;
}
   

}