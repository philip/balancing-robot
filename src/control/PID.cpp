#include "src/control/PID.h"
#include "src/math/Utilities.h"


using namespace math;

namespace control {

/*----------------------------------- P Controller -----------------------------------*/
controllerP::controllerP(float kP)
: m_kP(kP)
{
}

float controllerP::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    // output
    return m_kP*error;
}

/*----------------------------------- PD Controller ----------------------------------*/
controllerPD::controllerPD(float kP, float kD, float saturation)
: m_kP(kP)
, m_kD(kD)
, m_saturation(saturation)
, m_lastError(0)
, m_pt1FilterApply4(50.0f)
{
}

float controllerPD::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    float pTerm(m_kP*error);

    float dTerm((dT != 0.0f) ? m_kD*((error-m_lastError)/dT) : 0.0f);

    // Store error for next iteration
    m_lastError = error;

    dTerm = m_pt1FilterApply4.filter(dTerm, dT);

    float output(constrain(pTerm + dTerm, m_saturation));

    return output;
}

/*----------------------------------- Alternative PD Controller ----------------------------------*/
controllerPD2::controllerPD2(float kP, float kD, float saturation)
: m_kP(kP)
, m_kD(kD)
, m_saturation(saturation)
, m_inputTMinus1(0.0f)
, m_inputTMinus2(0.0f)
, m_setPointTMinus1(0.0f)
{
}

float controllerPD2::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    float pTerm(m_kP*error);

    float dTerm = + (m_kD*(setPoint - m_setPointTMinus1) - m_kD*(input - m_inputTMinus2))/dT;
    
    m_inputTMinus2 = m_inputTMinus1;
    m_inputTMinus1 = input;
    m_setPointTMinus1 = setPoint;
    return(pTerm + dTerm);
}

/*----------------------------------- PI Controller ----------------------------------*/
controllerPI::controllerPI(float kP, float kI, float saturation, float iTermSaturation)
: m_kP(kP)
, m_kI(kI)
, m_saturation(saturation)
, m_iTermSaturation(iTermSaturation)
, m_integrator(0)
{
}

float controllerPI::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    float pTerm(m_kPScale * m_kP * error);

    // Accumulate to the integrator
    m_integrator += error * dT;
    m_integrator = constrain(m_integrator, m_iTermSaturation);

    float iTerm = m_integrator * m_kIScale * m_kI;

    float output(constrain(pTerm + iTerm, m_saturation));

    return output;
}

void controllerPI::setGainScaling(float kPScale, float kIScale)
{
    static float scaleFactor(1.0f / 1000.0f);
    
    m_kPScale = (1000.0f + kPScale) * scaleFactor;
    m_kIScale = (1000.0f + kIScale) * scaleFactor;
}

void controllerPI::flushIntegral()
{
    m_integrator = 0;
}

/*----------------------------------- PID Controller ---------------------------------*/

controllerPID::controllerPID(float kP, float kI, float kD, float saturation, float iTermSaturation)
: m_kP(kP)
, m_kI(kI)
, m_kD(kD)
, m_kPScale(0.0f)
, m_kIScale(0.0f)
, m_kDScale(0.0f)
, m_saturation(saturation)
, m_lastError(0.0f)
, m_iTermSaturation(iTermSaturation)
, m_integrator(0.0f)
, m_pt1FilterApply4(20.0f)
{
}

float controllerPID::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    float pTerm(m_kPScale * m_kP * error);

    // Accumulate to the integrator
    m_integrator += error * dT;
    m_integrator = constrain(m_integrator, m_iTermSaturation);

    float iTerm = m_integrator * m_kIScale * m_kI;

    float dTerm((dT != 0.0f) ? m_kDScale * m_kD * ((error-m_lastError)/dT) : 0.0f);

    // Store error for next iteration
    m_lastError = error;

    dTerm = m_pt1FilterApply4.filter(dTerm, dT);

    float output(constrain(pTerm + iTerm + dTerm, m_saturation));

    return output;
}

void controllerPID::setGainScaling(float kPScale, float kIScale, float kDScale)
{
    static float scaleFactor(1.0f / 1000.0f);
    
    m_kPScale = (1000.0f + kPScale) * scaleFactor;
    m_kIScale = (1000.0f + kIScale) * scaleFactor;
    m_kDScale = (1000.0f + kDScale) * scaleFactor;
}

void controllerPID::flushIntegral()
{
    m_integrator = 0;
}

/*----------------------------------- PID Controller ---------------------------------*/

controllerPID2::controllerPID2(float kP, float kI, float kD, float saturation, float iTermSaturation)
: m_kP(kP)
, m_kI(kI)
, m_kD(kD)
, m_kPScale(0.0f)
, m_kIScale(0.0f)
, m_kDScale(0.0f)
, m_saturation(saturation)
, m_iTermSaturation(iTermSaturation)
, m_integrator(0.0f)
, m_inputTMinus1(0.0f)
, m_inputTMinus2(0.0f)
, m_setPointTMinus1(0.0f)
{
}

float controllerPID2::run(float dT, float input, float setPoint)
{
    float error(setPoint-input);

    float pTerm(m_kPScale * m_kP * error);

    // Accumulate to the integrator
    m_integrator += error * dT;
    m_integrator = constrain(m_integrator, m_iTermSaturation);

    float iTerm = m_integrator * m_kIScale * m_kI;

    float dTerm = + (m_kD*m_kDScale*((setPoint - m_setPointTMinus1) - (input - m_inputTMinus2)))/dT;
    
    m_inputTMinus2 = m_inputTMinus1;
    m_inputTMinus1 = input;
    m_setPointTMinus1 = setPoint;

    float output(constrain(pTerm + iTerm + dTerm, m_saturation));

    return output;
}

void controllerPID2::setGainScaling(float kPScale, float kIScale, float kDScale)
{
    static float scaleFactor(1.0f / 1000.0f);
    
    m_kPScale = (1000.0f + kPScale) * scaleFactor;
    m_kIScale = (1000.0f + kIScale) * scaleFactor;
    m_kDScale = (1000.0f + kDScale) * scaleFactor;
}

void controllerPID2::flushIntegral()
{
    m_integrator = 0;
}

}