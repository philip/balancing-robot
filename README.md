# RCNoster
This project aims at developing an open-source remote control (RC) for radio controlled aircrafts e.g. drones and airplanes in both software and hardware.

## Hardware
In the RC hobby they are numerous developers of flight controllers (FC). These boards are powerfull and very cheap and many hobbyists have either a spair or an old unit lying around. Many of these boards derive from the [CC3D Revolution](http://opwiki.readthedocs.io/en/latest/user_manual/revo/revo.html) board based on the STM32F405 processor

The goal is to provide open CAD and Circuit Board designs for full customization and choice in electronics.


## Software
This software builds on the real-time operating system (RTOS) [Zephyr](https://zephyrproject.org/) created by the Linux Foundation.
The fork [Zephyr-RC](https://github.com/philsson/zephyr-RC.git) is included as a module for better support of the [CC3D Revolution](http://opwiki.readthedocs.io/en/latest/user_manual/revo/revo.html).

A Visual Studio Code project is set up for development and configured to work under Linux.
For dependencies and zephyr specific documentation see [Zephyr Documentation](https://www.zephyrproject.org/doc/)